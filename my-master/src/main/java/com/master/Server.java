package com.master;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

public class Server {
    private int port;
    List<Client> clients;

    public Server(int port) {
        this.port = port;
        clients   = new LinkedList<>();
    }

    public void start() {
        try {
            ServerSocket serverSocket = new ServerSocket(this.port);
            System.out.println(Constants.STATUS + "Server started on port: " + this.port);
            while (true) {
                Socket connection = serverSocket.accept();
                Client client     = new Client(connection);
                clients.add(client);
                client.start();
                System.out.println(Constants.SUCCESS + "New connection accepted from " + connection.getInetAddress() + Constants.RESET);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
