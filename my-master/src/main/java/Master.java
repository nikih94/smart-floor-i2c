import com.master.Constants;
import com.pi4j.library.pigpio.PiGpio;

import java.util.stream.Stream;

/* remember, pigpiod must not run
 * 
must run with sudo

 cd /home/pi/Desktop/master ; sudo /usr/bin/env /usr/lib/jvm/java-11-openjdk-arm64/bin/java @/tmp/cp_a4wxmhfrr6f1ctz52vt84wbjp.argfile Master 

 */


 /*
  * set t5he communication speed (baudrate = bps = freq)

  sudo nano /boot/config.txt
 
    dtparam=i2c_baudrate=500000




  */

public class Master {

    private static int I2C_BUS = 1;

    private static int[] device_address = {10,14,15,19,21,22,12,16,8,5,23,1,4,9,2,3,30,26,25,29,13,18,20,20,27,6,24,11,17,7};
    private static int[] device_handle = new int[device_address.length];

    private static final int message_len = 12; // in bytes

    private static PiGpio piGpio;



    public static void main(String[] args) {
        device_address = Stream.of(args).mapToInt(Integer::parseInt).toArray();
        device_handle = new int[device_address.length];
        piGpio = PiGpio.newNativeInstance();

        piGpio.gpioInitialise();
        System.out.println("PIGPIO INITIALIZED");

        //System.out.println("PIGPIO VERSION   : " + piGpio.gpioVersion());
        //System.out.println("PIGPIO HARDWARE  : " + piGpio.gpioHardwareRevision());

        // open I2C channel/bus/device
        System.out.println("OPEN DEVICES");
        for(int i = 0 ; i < device_address.length; i++ ){
            device_handle[i] = piGpio.i2cOpen(I2C_BUS, device_address[i]);
            if(device_handle[i] < 0) {
                System.out.println("ERROR; I2C OPEN FAILED: ERROR CODE: " + device_handle[i] + " DEVICE ADDRESS: " + device_address[i]);
            }
        }

        System.out.println("");
        System.out.println("----------------------------------------");
        System.out.println("START I2C READING");
        System.out.println("----------------------------------------");

        int freq = 2; // frequency of measurements per second

        while(freq < 101){
            for (int j = 0 ; j < 10 ; j++ ){
                for(int i = 0 ; i < device_handle.length; i++ ){
                    readDevice(device_handle[i],device_address[i]);
                }
                try{
                    Thread.sleep(1000/freq);
                }catch(Exception e){}
            }
            //freq=freq*2;
            System.out.println("f: "+ freq);

        }
        

    }

    public static void readDevice(int handle, int device_address){
        byte[] readBuffer = new byte[message_len];
        int result = piGpio.i2cReadDevice(handle, readBuffer);
        if(result < 0) {
            //System.out.println("\nERROR; I2C READ FAILED: ERROR CODE: " + result);
            System.out.println(Integer.toString(device_address) +" " + Constants.RED_BACKGROUND+" " + Constants.RESET);
        }

        // validate read length
        if(result != message_len) {
            //System.out.println("\nERROR; I2C READ FAILED: LENGTH MISMATCH: " + result);
            System.out.println(Integer.toString(device_address) +" " + Constants.RED_BACKGROUND+" " + Constants.RESET);
        }
        System.out.print("Device: " + Integer.toString(device_address) + "--");//+ StringUtil.toHexString(device_address) + "--");
        //deserialize(readBuffer);
        printSensorData(deserialize(readBuffer));
    }

    public static int[] deserialize(byte[] buffer){
        int[] sensor_data = new int[8]; 
        int j;
        for(int i = 0 ; i < 8; i++){
            j = i + i / 2; 
            if(i % 2 == 0){
                sensor_data[i] = (buffer[j] & 0xff) << 4;
                int val = (buffer[j+1] & 0xff) >>> 4;
                sensor_data[i] += val;

            }else{
                sensor_data[i] = (buffer[j] << 4 ) & 0xff;
                sensor_data[i] = sensor_data[i] << 4;
                sensor_data[i] += buffer[j+1] & 0xff;
            }
        }

        return sensor_data;

    }

    public static void printSensorData(int[] sensor_data){
        for(int i = 0; i < sensor_data.length; i++){
            System.out.print(sensor_data[i]+" , ");
        }
        System.out.println(Constants.GREEN_BACKGROUND+" "+ Constants.RESET);
        System.out.println();
    }

    
}
