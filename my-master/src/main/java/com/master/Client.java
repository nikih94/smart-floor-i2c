package com.master;

import java.io.*;
import java.net.Socket;

public class Client extends Thread {
    private String          controllerId;
    private Socket          socket;
    private DataInputStream inputStream;
    private int             readLength;

    public Client(String controllerId, Socket socket) {
        this.controllerId = controllerId;
        this.socket       = socket;
    }

    public Client(Socket socket) throws IOException {
        this.socket       = socket;
        this.controllerId = "";
        this.readLength   = 12;
        this.inputStream  = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
    }

    public void run() {
        try {
            byte[] idData = inputStream.readNBytes(17);
            this.controllerId = new String(idData).replaceAll("\\r\\n", "");
        } catch (IOException e) {
            System.out.println(Constants.ERROR + " protocol violation on controllerId");
            closeConnection();
            e.printStackTrace();
        }
        while (true) {
            byte[] buffer = new byte[readLength];
            try {
                inputStream.read(buffer);
                int data[] = deserialize(buffer);
                printSensorData(data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void closeConnection() {
        try {
            inputStream.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int[] deserialize(byte[] buffer) {
        int[] sensor_data = new int[8];
        int   j;
        for (int i = 0; i < 8; i++) {
            j = i + i / 2;
            if (i % 2 == 0) {
                sensor_data[i] = (buffer[j] & 0xff) << 4;
                int val = (buffer[j + 1] & 0xff) >>> 4;
                sensor_data[i] += val;

            } else {
                sensor_data[i] = (buffer[j] << 4) & 0xff;
                sensor_data[i] = sensor_data[i] << 4;
                sensor_data[i] += buffer[j + 1] & 0xff;
            }
        }
        return sensor_data;
    }

    public void printSensorData(int[] sensor_data) {
        System.out.print(controllerId + " ");
        for (int i = 0; i < sensor_data.length; i++) {
            System.out.print(sensor_data[i] + " , ");
        }
        System.out.println();
    }
}

